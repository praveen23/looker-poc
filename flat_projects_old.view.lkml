view: flat_projects_old {
  view_label: "Projects"
  sql_table_name: dashboard_tables.flat_projects ;;

  dimension_group: awaiting_10_percent {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.awaiting_10_percent_date ;;
  }

  dimension_group: briefing_done {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.briefing_done_date ;;
  }

  dimension: city_id {
    group_label: "IDs"
    type: number
    sql: ${TABLE}.city_id ;;
  }

  dimension_group: design_in_progress {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.design_in_progress ;;
  }

  dimension: dp_source {
    type: string
    sql: ${TABLE}.dp_source ;;

  }

  dimension: fifty_percent_collected {
    type: number
    sql: ${TABLE}.fifty_percent_collected ;;
  }

  dimension_group: fifty_percent_collected {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.fifty_percent_collected_date ;;
  }

  dimension: gmv_amount {
    type: number
    sql: ${TABLE}.gmv_amount ;;
  }

  dimension: gmv_month {
    type: string
    sql: ${TABLE}.gmv_month ;;
  }

  dimension: is_awaiting_ten_percent {
    type: yesno
    sql: ${TABLE}.is_awaiting_ten_percent = 1 ;;
  }

  dimension: is_converted {
    type: yesno
    sql: ${TABLE}.is_converted = 1 ;;
  }

  dimension: is_proposal_present {
    type: yesno
    sql: ${TABLE}.is_proposal_present = 1;;
  }

  dimension: is_qualified {
    type: yesno
    sql: ${TABLE}.is_qualified = 1 ;;
  }

  dimension: order_confirmed {
    type: number
    sql: ${TABLE}.order_confirmed ;;
  }

  dimension_group: order_confirmed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_confirmed_date ;;
  }

  dimension: primary_cm {
    label: "CM"
    type: string
    sql: ${TABLE}.primary_cm ;;
  }

  dimension: primary_cm_id {
    group_label: "IDs"
    type: number
    sql: ${TABLE}.primary_cm_id ;;
  }

  dimension: primary_designer {
    label: "Designer"
    type: string
    sql: ${TABLE}.primary_designer ;;
  }

  dimension: primary_designer_id {
    group_label: "IDs"
    type: number
    sql: ${TABLE}.primary_designer_id ;;
  }

  dimension: primary_gm {
    label: "Primary GM"
    type: string
    sql: ${TABLE}.primary_gm ;;
    link: {
      url: "/dashboards/8?GM%20Name={{ value }}"
      icon_url: "http://www.google.com/s2/favicons?domain=www.livspace.com"
      label: "{{value}} - GM Lookup"
    }

  }

  dimension: primary_gm_id {
    group_label: "IDs"
    type: number
    sql: ${TABLE}.primary_gm_id ;;
  }

  dimension: project_city {
    group_label: "Location"
    label: "City"
    type: string
    sql: Coalesce(${TABLE}.project_city,'Other') ;;
    drill_fields: [city_locations_pdt.location]
  }

  # dimension: region {
#     group_label: "Location"
  #   type: string
  #   map_layer_name: india_regions
  #   sql:
  #   CASE
  #     WHEN ${project_city} = 'Bangalore' THEN 'Karnataka'
  #     WHEN ${project_city} = 'Mumbai' THEN 'Maharashtra'
  #     WHEN ${project_city} = 'Noida' THEN 'Uttar Pradesh'
  #     WHEN ${project_city} = 'Gurgaon' THEN 'Haryana and Punjab'
  #     WHEN ${project_city} = 'Delhi' THEN 'Delhi'
  #     WHEN ${project_city} = 'Hyderabad' THEN 'Andhra Pradesh'
  #     WHEN ${project_city} = 'Pune' THEN 'Maharashtra'
  #     WHEN ${project_city} = 'Thane' THEN 'Maharashtra'
  #     WHEN ${project_city} = 'Chennai' THEN 'Tamil Nadu'
  #     WHEN ${project_city} = 'Ghaziabad' THEN 'Uttar Pradesh'
  #     WHEN ${project_city} = 'Faridabad' THEN 'Haryana'
  #     WHEN ${project_city} = 'Navi Mumbai' THEN 'Maharashtra'
  #     WHEN ${project_city} = 'Dwarka' THEN 'Gujarat'
  #     ELSE 'Other'
  #   END
  #   ;;
  #   drill_fields: [project_city]
  # }

  dimension_group: project_created {
    label: "Created"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.project_created_at ;;
  }

  dimension: project_id {
    primary_key: yes
    group_label: "IDs"
    type: number
    sql: ${TABLE}.project_id ;;
  }

  dimension: project_source {
    label: "Source"
    type: string
    sql: ${TABLE}.project_source ;;

  }

  dimension: project_source_id {
    group_label: "IDs"
    type: number
    sql: ${TABLE}.project_source_id ;;
  }

  dimension_group: prospective_lead {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.prospective_lead_date ;;
  }

  dimension: stage {
    label: "Stage"
    type: string
    sql: ${TABLE}.stage ;;

  }

  dimension: stage_id {
    group_label: "IDs"
    type: number
    sql: ${TABLE}.stage_id ;;
  }

  dimension: status {
    description: "Active, Inactive, Onhold"
    type: string
    sql: INITCAP(${TABLE}.status) ;;

  }

  dimension: ten_percent_collected {
    type: number
    sql: ${TABLE}.ten_percent_collected ;;
  }

  dimension_group: ten_percent_collected {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ten_percent_collected_date ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      hour,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: weight {
    type: number
    sql: ${TABLE}.weight ;;
  }

####### SQL STATEMENTS LOGIC - CANVAS DASH ################################################

  measure: total_projects {
    group_label: "Conversion Funnel"
    label: "1) Total Projects"
    type: count
    drill_fields: [detail*]
  }

  measure: total_projects_1{
    label: "Total Projects"
    sql: ${total_projects} ;;
    type: number
    drill_fields: [detail*]
  }

  measure: total_qualified {
    group_label: "Conversion Funnel"
    label: "2) Total Qualified"
    type: count
    filters: {
      field: is_qualified
      value: "yes"
    }
    drill_fields: [detail*]
  }

  measure: percentage_qualified {
    type: number
    description: "Total Qualified Projects over Total Projects"
    value_format_name: percent_2
    sql: 1.0*${total_qualified}/NULLIF(${total_projects},0) ;;
    drill_fields: [detail*]
  }

  measure: total_proposals_sent {
    group_label: "Conversion Funnel"
    label: "3) Total Proposals Sent"
    type: count
    filters: {
      field: is_proposal_present
      value: "yes"
    }
    drill_fields: [detail*]
  }

  measure: pitched_percentage {
    type: number
    description: "Total Proposals Sent over Total Qualified Projects"
    value_format_name: percent_2
    sql: 1.0*${total_proposals_sent}/NULLIF(${total_qualified},0) ;;
    drill_fields: [detail*]
  }

  measure: awaiting_ten_percent {
    label: "Awaiting 10%"
    type: count
    filters: {
      field: is_awaiting_ten_percent
      value: "yes"
    }
    drill_fields: [detail*]
  }

  measure: total_converted {
    group_label: "Conversion Funnel"
    label: "4) Total Converted"
    type: count
    filters: {
      field: is_converted
      value: "yes"
    }
    drill_fields: [detail*]
  }

  measure: conversion_percentage {
    group_label: "Conversion"
    description: "Total Converted Projects over Total Proposals Sent"
    type: number
    value_format_name: percent_2
    sql: 1.0*${total_converted}/NULLIF(${total_proposals_sent},0) ;;
    drill_fields: [detail*]
  }

  dimension: cancellation_phase{
    description:
      "Incoming Lead: Weight 100 to 120
      Briefing Phase: Weight 150 to 300
      Design Phase: Weight 400 to 500
      Post Design Phase: Weight 550 to 700
      Closed: Weight more than 800"
    type: string
    sql:
        CASE
          WHEN ${weight} >= 400 AND ${weight} <= 500 THEN 'Design Phase'
          WHEN ${weight} >= 150 AND ${weight} <= 300 THEN 'Briefing Phase'
          WHEN ${weight} >= 100 AND ${weight} <= 120 THEN 'Incoming Lead'
          WHEN ${weight} >= 550 AND ${weight} <= 700 THEN 'Post Design Phase'
          WHEN ${weight} >= 800 THEN 'Closed'
          ELSE 'Other'
        END
    ;;
    drill_fields: [project_city]

  }

  measure: inactive_project_count {
    type: count
    drill_fields: [detail*]
  }


  set: detail {
    fields: [project_created_date,primary_gm,primary_designer,primary_cm,stage,status]
  }


}
