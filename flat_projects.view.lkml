view: flat_projects {
  sql_table_name: dashboard_tables.flat_projects ;;

  dimension_group: awaiting_10_percent {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.awaiting_10_percent_date ;;
  }

  dimension_group: briefing_done {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.briefing_done_date ;;
  }

  dimension: budget {
    type: number
    sql: ${TABLE}.budget ;;
  }

  dimension: city_id {
    type: number
    sql: ${TABLE}.city_id ;;
  }

  dimension_group: design_in_progress {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.design_in_progress ;;
  }

  dimension: dp_source {
    type: string
    sql: ${TABLE}.dp_source ;;
  }

  dimension: fifty_percent_amount {
    type: number
    sql: ${TABLE}.fifty_percent_amount ;;
  }

  dimension: fifty_percent_collected {
    type: number
    sql: ${TABLE}.fifty_percent_collected ;;
  }

  dimension_group: fifty_percent_collected {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.fifty_percent_collected_date ;;
  }

  dimension: full_amount {
    type: number
    sql: ${TABLE}.full_amount ;;
  }

  dimension: gmv_amount {
    type: number
    sql: ${TABLE}.gmv_amount ;;
  }

  dimension: gmv_month {
    type: string
    sql: ${TABLE}.gmv_month ;;
  }

  dimension: inactivation_reason {
    type: string
    sql: ${TABLE}.inactivation_reason ;;
  }

  dimension: is_awaiting_ten_percent {
    type: number
    sql: ${TABLE}.is_awaiting_ten_percent ;;
  }

  dimension: is_converted {
    type: number
    sql: ${TABLE}.is_converted ;;
  }

  dimension: is_proposal_present {
    type: number
    sql: ${TABLE}.is_proposal_present ;;
  }

  dimension: is_qualified {
    type: number
    sql: ${TABLE}.is_qualified ;;
  }

  dimension: order_confirmed {
    type: number
    sql: ${TABLE}.order_confirmed ;;
  }

  dimension_group: order_confirmed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_confirmed_date ;;
  }

  dimension: primary_cm {
    type: string
    sql: ${TABLE}.primary_cm ;;
  }

  dimension: primary_cm_id {
    type: string
    sql: ${TABLE}.primary_cm_id ;;
  }

  dimension: primary_designer {
    type: string
    sql: ${TABLE}.primary_designer ;;
  }

  dimension: primary_designer_id {
    type: string
    sql: ${TABLE}.primary_designer_id ;;
  }

  dimension: primary_gm {
    type: string
    sql: ${TABLE}.primary_gm ;;
  }

  dimension: primary_gm_id {
    type: string
    sql: ${TABLE}.primary_gm_id ;;
  }

  dimension: project_city {
    group_label: "Location"
    label: "City"
    type: string
    sql: Coalesce(${TABLE}.project_city, 'Other') ;;
    drill_fields: [city_locations_pdt.location]
  }

  dimension: region {
  group_label: "Location"
  type: string
  map_layer_name: india_regions
  sql:
  CASE
    WHEN ${project_city} = 'Bangalore' THEN 'Karnataka'
    WHEN ${project_city} = 'Mumbai' THEN 'Maharashtra'
    WHEN ${project_city} = 'Noida' THEN 'Uttar Pradesh'
    WHEN ${project_city} = 'Gurgaon' THEN 'Haryana and Punjab'
    WHEN ${project_city} = 'Delhi' THEN 'Delhi'
    WHEN ${project_city} = 'Hyderabad' THEN 'Andhra Pradesh'
    WHEN ${project_city} = 'Pune' THEN 'Maharashtra'
    WHEN ${project_city} = 'Thane' THEN 'Maharashtra'
    WHEN ${project_city} = 'Chennai' THEN 'Tamil Nadu'
    WHEN ${project_city} = 'Ghaziabad' THEN 'Uttar Pradesh'
    WHEN ${project_city} = 'Faridabad' THEN 'Haryana'
    WHEN ${project_city} = 'Navi Mumbai' THEN 'Maharashtra'
    WHEN ${project_city} = 'Dwarka' THEN 'Gujarat'
    ELSE 'Other'
  END
  ;;
  drill_fields: [project_city]
  }

  dimension_group: project_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.project_created_at ;;
  }

  dimension: project_id {
    type: number
    sql: ${TABLE}.project_id ;;
  }

  dimension: project_prority {
    type: string
    sql: ${TABLE}.project_prority ;;
  }

  dimension: project_source {
    type: string
    sql: ${TABLE}.project_source ;;
  }

  dimension: project_source_id {
    type: number
    sql: ${TABLE}.project_source_id ;;
  }

  dimension: property_type {
    type: string
    sql: ${TABLE}.property_type ;;
  }

  dimension_group: prospective_lead {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.prospective_lead_date ;;
  }

  dimension: stage {
    type: string
    sql: ${TABLE}.stage ;;
  }

  dimension: stage_id {
    type: number
    sql: ${TABLE}.stage_id ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: ten_percent_amount {
    type: number
    sql: ${TABLE}.ten_percent_amount ;;
  }

  dimension: ten_percent_collected {
    type: number
    sql: ${TABLE}.ten_percent_collected ;;
  }

  dimension_group: ten_percent_collected {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ten_percent_collected_date ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: weight {
    type: number
    sql: ${TABLE}.weight ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }

  measure: total_projects {
    group_label: "Conversion Funnel"
    label: "1) Total Projects"
    type: count
    drill_fields: [detail*]
  }

  measure: total_projects_1{
    label: "Total Projects"
    sql: ${total_projects} ;;
    type: number
    drill_fields: [detail*]
  }

  measure: total_qualified {
    group_label: "Conversion Funnel"
    label: "2) Total Qualified"
    type: count
    filters: {
      field: is_qualified
      value: "1"
    }
    drill_fields: [detail*]
  }

  measure: percentage_qualified {
    type: number
    description: "Total Qualified Projects over Total Projects"
    value_format_name: percent_2
    sql: 1.0*${total_qualified}/NULLIF(${total_projects},0) ;;
    drill_fields: [detail*]
  }

  measure: total_proposals_sent {
    group_label: "Conversion Funnel"
    label: "3) Total Proposals Sent"
    type: count
    filters: {
      field: is_proposal_present
      value: "1"
    }
    drill_fields: [detail*]
  }

  measure: pitched_percentage {
    type: number
    description: "Total Proposals Sent over Total Qualified Projects"
    value_format_name: percent_2
    sql: 1.0*${total_proposals_sent}/NULLIF(${total_qualified},0) ;;
    drill_fields: [detail*]
  }

  measure: awaiting_ten_percent {
    label: "Awaiting 10%"
    type: count
    filters: {
      field: is_awaiting_ten_percent
      value: "1"
    }
    drill_fields: [detail*]
  }

  measure: total_converted {
    group_label: "Conversion Funnel"
    label: "4) Total Converted"
    type: count
    filters: {
      field: is_converted
      value: "1"
    }
    drill_fields: [detail*]
  }

  measure: conversion_percentage {
    group_label: "Conversion"
    description: "Total Converted Projects over Total Proposals Sent"
    type: number
    value_format_name: percent_2
    sql: 1.0*${total_converted}/NULLIF(${total_proposals_sent},0) ;;
    drill_fields: [detail*]
  }

  dimension: cancellation_phase{
    description:
    "Incoming Lead: Weight 100 to 120
    Briefing Phase: Weight 150 to 300
    Design Phase: Weight 400 to 500
    Post Design Phase: Weight 550 to 700
    Closed: Weight more than 800"
    type: string
    sql:
      CASE
        WHEN ${weight} >= 400 AND ${weight} <= 500 THEN 'Design Phase'
        WHEN ${weight} >= 150 AND ${weight} <= 300 THEN 'Briefing Phase'
        WHEN ${weight} >= 100 AND ${weight} <= 120 THEN 'Incoming Lead'
        WHEN ${weight} >= 550 AND ${weight} <= 700 THEN 'Post Design Phase'
        WHEN ${weight} >= 800 THEN 'Closed'
        ELSE 'Other'
      END
  ;;
    drill_fields: [project_city]

  }

  measure: inactive_project_count {
    type: count
    drill_fields: [detail*]
  }



  set: detail {
    fields: [project_created_date,primary_gm,primary_designer,primary_cm,stage,status]
  }

  measure: avg_bc_done {
    type: average
    sql: DATEDIFF( day, ${project_created_date}, ${briefing_done_date}) ;;
    drill_fields: [detail*]
  }

  dimension: 7_day_BC_rate{
    description:""
    type: string
    sql:
      CASE
        WHEN DATEDIFF( day, ${project_created_date}, ${briefing_done_date}) <=7 then 1 else 0
      END
  ;;


  }
  dimension: 14_day_BC_rate{
    description:""
    type: string
    sql:
      CASE
        WHEN DATEDIFF( day, ${project_created_date}, ${briefing_done_date}) >=7
        and DATEDIFF( day, ${project_created_date}, ${briefing_done_date}) <=14 then 1 else 0
      END
  ;;


    }

}
