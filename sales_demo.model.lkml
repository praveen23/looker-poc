connection: "livspacereportsflydata"

# include all the views
include: "*.view"

datagroup: sales_demo_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: sales_demo_default_datagroup

map_layer:  india_regions {
  file: "india_states.json"
}

explore: flat_projects {}
